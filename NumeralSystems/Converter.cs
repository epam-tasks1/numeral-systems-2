﻿using System;
using System.Globalization;

namespace NumeralSystems
{
    public static class Converter
    {
        public static int ParsePositiveFromOctal(this string source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int decimalNumber;
            
            if (!TryParseByRadix(source, 8, out decimalNumber))
            {
                throw new ArgumentException("Source is wrong.");
            }

            if (decimalNumber < 0)
            {
                throw new ArgumentException("Source is negative.");
            }

            return decimalNumber;
        }

        public static int ParsePositiveFromDecimal(this string source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int decimalNumber;

            if (!TryParseByRadix(source, 10, out decimalNumber) || source[0] == '-')
            {
                throw new ArgumentException("Source is wrong or negative.");
            }

            return decimalNumber;
        }

        public static int ParsePositiveFromHex(this string source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int decimalNumber;
            
            if (!TryParseByRadix(source, 16, out decimalNumber))
            {
                throw new ArgumentException("Source is wrong.");
            }

            if (decimalNumber < 0)
            {
                throw new ArgumentException("Source is negative.");
            }

            return decimalNumber;
        }

        public static int ParsePositiveByRadix(this string source, int radix)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int decimalNumber;

            if (!TryParseByRadix(source, radix, out decimalNumber))
            {
                throw new ArgumentException("Source is wrong.");
            }

            if (decimalNumber < 0)
            {
                throw new ArgumentException("Source is negative.");
            }

            return decimalNumber;
        }

        public static int ParseByRadix(this string source, int radix)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int decimalNumber;

            if (!TryParseByRadix(source, radix, out decimalNumber))
            {
                throw new ArgumentException("Source is wrong.");
            }

            return decimalNumber;
        }

        public static bool TryParsePositiveFromOctal(this string source, out int value)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int decimalNumber;

            if (!TryParseByRadix(source, 8, out decimalNumber))
            {
                value = 0;
                return false;
            }

            if (decimalNumber < 0)
            {
                value = 0;
                return false;
            }

            value = decimalNumber;
            return true;
        }

        public static bool TryParsePositiveFromDecimal(this string source, out int value)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int decimalNumber;

            if (!TryParseByRadix(source, 10, out decimalNumber))
            {
                value = 0;
                return false;
            }

            if (decimalNumber < 0)
            {
                value = 0;
                return false;
            }

            value = decimalNumber;
            return true;
        }

        public static bool TryParsePositiveFromHex(this string source, out int value)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int decimalNumber;

            if (!TryParseByRadix(source, 16, out decimalNumber))
            {
                value = 0;
                return false;
            }

            if (decimalNumber < 0)
            {
                value = 0;
                return false;
            }

            value = decimalNumber;
            return true;
        }

        public static bool TryParsePositiveByRadix(this string source, int radix, out int value)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int decimalNumber;

            if (!TryParseByRadix(source, radix, out decimalNumber))
            {
                value = 0;
                return false;
            }

            if (decimalNumber < 0)
            {
                value = 0;
                return false;
            }

            value = decimalNumber;
            return true;
        }

        public static bool TryParseByRadix(this string source, int radix, out int value)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            source = source.ToUpper(CultureInfo.CurrentCulture);
            string binaryNumber;

            if (radix == 8)
            {
                char[] octalArray = new char[] { '0', '1', '2', '3', '4', '5', '6', '7' };
                string[] octalBinaryArray = new string[] { "000", "001", "010", "011", "100", "101", "110", "111" };

                if (!ConvertNumeralSystemToBinary(source, octalArray, octalBinaryArray, out binaryNumber))
                {
                    value = 0;
                    return false;
                }

                value = ConvertBinaryToDecimal(binaryNumber);
                return true;
            }

            if (radix == 10)
            {
                int decimalNumber;

                if (!ConvertDecimalToDecimal(source, out decimalNumber))
                {
                    value = 0;
                    return false;
                }

                value = decimalNumber;
                return true;
            }

            if (radix == 16)
            {
                char[] hexArray = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
                string[] hexBinaryArray = new string[] { "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111" };

                if (!ConvertNumeralSystemToBinary(source, hexArray, hexBinaryArray, out binaryNumber))
                {
                    value = 0;
                    return false;
                }

                value = ConvertBinaryToDecimal(binaryNumber);
                return true;
            }

            throw new ArgumentException("Radix is incorrect.");
        }

        public static bool ConvertNumeralSystemToBinary(string sourceNumber, char[] numeralSystemArray, string[] binaryArray, out string binaryNumber)
        {
            if (sourceNumber is null)
            {
                throw new ArgumentNullException(nameof(sourceNumber));
            }

            if (numeralSystemArray is null)
            {
                throw new ArgumentNullException(nameof(numeralSystemArray));
            }

            if (binaryArray is null)
            {
                throw new ArgumentNullException(nameof(binaryArray));
            }

            binaryNumber = string.Empty;

            for (int i = 0; i < sourceNumber.Length; i++)
            {
                bool numberIsCorrect = false;
                for (int j = 0; j < numeralSystemArray.Length; j++)
                {
                    if (sourceNumber[i] == numeralSystemArray[j])
                    {
                        binaryNumber = string.Concat(binaryNumber, binaryArray[j]);
                        numberIsCorrect = true;
                        break;
                    }
                }

                if (!numberIsCorrect)
                {
                    binaryNumber = string.Empty;
                    return false;
                }
            }

            return true;
        }

        public static bool ConvertDecimalToDecimal(string sourceNumber, out int decimalNumber)
        {
            if (sourceNumber is null)
            {
                throw new ArgumentNullException(nameof(sourceNumber));
            }

            bool numberIsNegative = false;

            if (sourceNumber[0] == '-')
            {
                sourceNumber = sourceNumber[1..];
                numberIsNegative = true;
            }

            int decimalValue = 0;
            char[] decimalStringArray = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            int[] decimalIntArray = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            for (int i = sourceNumber.Length - 1; i >= 0; i--)
            {
                bool numberIsCorrect = false;
                for (int j = 0; j < decimalStringArray.Length; j++)
                {
                    if (sourceNumber[i] == decimalStringArray[j])
                    {
                        decimalValue += decimalIntArray[j] * (int)Math.Pow(10, sourceNumber.Length - i - 1);
                        numberIsCorrect = true;
                        break;
                    }
                }

                if (!numberIsCorrect)
                {
                    decimalNumber = 0;
                    return false;
                }
            }

            decimalNumber = numberIsNegative ? decimalValue * -1 : decimalValue;
            return true;
        }

        public static int ConvertBinaryToDecimal(string binaryNumber)
        {
            if (binaryNumber is null)
            {
                throw new ArgumentNullException(nameof(binaryNumber));
            }

            if (binaryNumber.Length > 32)
            {
                binaryNumber = binaryNumber[(binaryNumber.Length - 32) ..];
            }

            if (binaryNumber.Length < 32)
            {
                binaryNumber = string.Concat(new string('0', 32 - binaryNumber.Length), binaryNumber);
            }

            bool numberIsNegative = false;

            if (binaryNumber[0] == '1')
            {
                numberIsNegative = true;
            }

            if (numberIsNegative)
            {
                binaryNumber = NegativeBinaryToPositive(binaryNumber);
            }

            int decimalNumber = 0;

            for (int i = binaryNumber.Length - 1; i >= 0; i--)
            {
                if (binaryNumber[i] == '0')
                {
                    continue;
                }

                decimalNumber += (int)Math.Pow(2, 31 - i);
            }

            return numberIsNegative ? decimalNumber * -1 : decimalNumber;
        }

        public static string NegativeBinaryToPositive(string negativeBinary)
        {
            if (negativeBinary is null)
            {
                throw new ArgumentNullException(nameof(negativeBinary));
            }

            char[] binaryNumberArray = negativeBinary.ToCharArray();

            for (int i = 0; i < binaryNumberArray.Length; i++)
            {
                if (binaryNumberArray[i] == '0')
                {
                    binaryNumberArray[i] = '1';
                    continue;
                }

                binaryNumberArray[i] = '0';
            }

            for (int i = binaryNumberArray.Length - 1; i >= 0; i--)
            {
                if (binaryNumberArray[i] == '0')
                {
                    binaryNumberArray[i] = '1';
                    break;
                }

                binaryNumberArray[i] = '0';
            }

            return new string(binaryNumberArray);
        }
    }
}
